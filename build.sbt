name := "dreamer"
organization := "org.danielwojda"
version := "0.0.1"

assemblyJarName in assembly := s"${name.value}-${version.value}.jar"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11",
  "com.typesafe.akka" %% "akka-http-testkit" % "2.4.11" % Test,
  "org.scalatest" %% "scalatest" % "3.0.0" % Test
)
