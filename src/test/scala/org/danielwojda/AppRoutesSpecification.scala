package org.danielwojda

import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FlatSpec, Matchers}

class AppRoutesSpecification extends FlatSpec with Matchers with ScalatestRouteTest {

  val appRoutes = new AppRoutes{}.routes

  "The App" should "provides '/status' endpoint" in {
    //when
    Get("/status") ~> appRoutes ~> check {
      //then
      handled shouldBe true
    }
  }

  "Status endpoint" should "return 200 Http Status and 'OK' as a body" in {
    //when
    Get("/status") ~> appRoutes ~> check {
      //then
      status.intValue shouldEqual 200
      responseAs[String] shouldBe "OK"
    }
  }

}
