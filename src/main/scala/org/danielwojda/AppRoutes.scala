package org.danielwojda

import akka.http.scaladsl.server.Directives._

trait AppRoutes {

  val routes = get {
    path("status") {
      complete("OK")
    }
  }
}
