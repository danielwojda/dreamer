package org.danielwojda

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

object DreamerApp extends App with AppRoutes {

  implicit val system = ActorSystem("dreamer-app")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  val port = 5000

  val bindingFuture = Http().bindAndHandle(routes, "localhost", port)

  println(
    s"""
      |**************************************
      |Server online at http://localhost:$port
      |**************************************
    """.stripMargin
  )
}
